﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    Camera cam;
    float sensitivity = 200f;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // move
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        // mouse look
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;

        Camera.current.transform.Rotate(Vector2.up * mouseX);
        Camera.current.transform.Rotate(Vector2.left * mouseY);
        Camera.current.transform.Translate(new Vector3(x, 0, z));
    }

    // Reference : https://www.youtube.com/watch?v=_QajrabyTJc
}
