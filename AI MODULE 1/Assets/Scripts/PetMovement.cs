﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PetMovement : MonoBehaviour
{
    public Transform goal;
    public float speed;
    public float rotSpeed;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate() // physics code inside Update(); movement code inside LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);

        //transform.LookAt(goal);
        Vector3 direction = lookAtGoal - transform.position;

        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);

        if (Vector3.Distance(lookAtGoal, transform.position) > 1)
            transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
