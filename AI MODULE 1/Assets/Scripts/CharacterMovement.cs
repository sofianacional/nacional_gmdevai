﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public Vector3 move;
    public float rotSpeed;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //move = new Vector3(0, 0, Input.GetAxis("Vertical"));
        //transform.eulerAngles += new Vector3(0, Input.GetAxis("Horizontal"), 0) * rotSpeed;

        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(Input.mousePosition), Time.deltaTime * rotSpeed);
        transform.eulerAngles += new Vector3(transform.rotation, transform.LookAt(Input.mousePosition), Time.deltaTime * rotSpeed);

        this.transform.Translate(move * speed * Time.deltaTime);
    }
}
