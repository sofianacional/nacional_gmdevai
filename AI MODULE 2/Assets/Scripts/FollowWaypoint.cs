﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FollowWaypoint : MonoBehaviour
{
    public UnityStandardAssets.Utility.WaypointCircuit circuit;
    //public GameObject[] waypoints;
    int currentWaypointIndex = 0;

    public float speed = 15;
    float rotSpeed = 3;
    float accuracy = 1;
    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    void LateUpdate()
    {
        if (circuit.Waypoints.Length == 0) return;

        GameObject currentWaypoint = circuit.Waypoints[currentWaypointIndex].gameObject;
        Vector3 lookAtGoal = new Vector3(currentWaypoint.transform.position.x, this.transform.position.y,
                             currentWaypoint.transform.position.z);
        Vector3 direction = lookAtGoal - this.transform.position;

        if(direction.magnitude < 1)
        {
            currentWaypointIndex++;
            if(currentWaypointIndex >= circuit.Waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction),
                                  rotSpeed * Time.deltaTime);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
