﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	
	void OnCollisionEnter(Collision col)
    {
		if(col.gameObject.CompareTag("player") || col.gameObject.CompareTag("tank"))
        {
			GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);

			Debug.Log(col.gameObject);
			col.gameObject.GetComponent<Health>().curHP--;

			Destroy(e, 1.5f);
			
		}
		Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
