﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Slider bar;
    public int maxHP;
    public int curHP;
    public bool isDead;

    // Update is called once per frame
    void Update()
    {
        bar.value = curHP;
        
        if (curHP <= 0)
        {
            Debug.Log("dead player");
            isDead = true;
            Destroy(this.gameObject);
        }
            
    }
}
