﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    public Animator anim;
    public GameObject player;

    public GameObject bullet;
    public GameObject turret;

    public GameObject GetPlayer()
    {
        return player;
    }

    private void Start()
    {
        anim = this.GetComponent<Animator>();
    }
    private void Update()
    {
        if (anim.GetBool("isDead"))
            return;

        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetInteger("tankHP", this.gameObject.GetComponent<Health>().curHP);
    }

    void Fire()
    {
        if (player.GetComponent<Health>().isDead)
        {
            Debug.Log("dead");
            anim.SetBool("isDead", true);
            return;
        }

        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);

    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }
    public void StopFiring()
    {
         CancelInvoke("Fire");
    }
}
