﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    GameObject[] goals;
    NavMeshAgent agent;
    Animator anim;
    float speedMultiplier;

    float detectionRadius = 10;
    float fleeRadius = 10;
    void ResetAgent()
    {
        speedMultiplier = Random.Range(0.5f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        anim.SetFloat("speedMultiplier", speedMultiplier);
        anim.SetTrigger("isWalking");
        agent.ResetPath();
    }

    void Start()
    {
        goals = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        anim = this.GetComponent<Animator>();

        agent.SetDestination(goals[Random.Range(0, goals.Length)].transform.position);
        anim.SetFloat("wOffset", Random.Range(0.1f, 1f));

        ResetAgent();
    }

    void LateUpdate()
    {
        if(agent.remainingDistance < 1)
        {
            agent.SetDestination(goals[Random.Range(0, goals.Length)].transform.position);
        }
    }

    public void DetectNewObstacle(Vector3 location, GameObject obstacle)
    {
        if(Vector3.Distance(location, this.transform.position) < detectionRadius)
        {
            Vector3 direction;

            if (obstacle.CompareTag("Green")) // flee
            {
                direction = (this.transform.position - location).normalized;
            }
            else // flock
            {
                direction = location.normalized;
            }
            
            Vector3 newGoal = this.transform.position + direction * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if(path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);

                anim.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        } 
    }
}
